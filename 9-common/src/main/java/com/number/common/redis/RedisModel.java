package com.number.common.redis;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jiang yong tao
 * @date 2018/9/18  10:15
 */
@Data
public class RedisModel implements Serializable {

    /**
     * redis中的key
     */
    private String redisKey;
    /**
     * 姓名
     */
    private String name;
    /**
     * 电话
     */
    private String tel;
    /**
     * 住址
     */
    private String address;


}
