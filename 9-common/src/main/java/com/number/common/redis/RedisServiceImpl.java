package com.number.common.redis;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author jiang yong tao
 * @date 2018/9/18  9:37
 */
@Service
public class RedisServiceImpl implements RedisService{

    @Autowired
    private RedisTemplate redisTemplate;



    /**
     * 一天有多少分钟，默认时间是一天
     */
    private static final Integer MINUTES_OF_ONE_DAY = 24 * 60;

    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 批量删除对应的value
     *
     * @param keys
     */
    @Override
    public void remove(final String... keys) {
        for (String key : keys) {
            remove(key);
        }
    }

    /**
     * 批量删除key
     *
     * @param pattern
     */
    @Override
    public void removePattern(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0) {
            redisTemplate.delete(keys);
        }
    }

    /**
     * 删除对应的value
     *
     * @param key
     */
    @Override
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * 判断缓存中是否有对应的value
     *
     * @param key
     * @return
     */
    @Override
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 更新缓存时间
     * @param key
     * @param expireTime
     */
    @Override
    public void refreshExpireTime(final String key,Integer expireTime)
    {
        if(exists(key)) {
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
        }
    }
    /**
     * 读取缓存
     *
     * @param key
     * @return
     */
    @Override
    public Object get(final String key) {
        Object result = null;
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        result = operations.get(key);
        return result;
    }

    /**
     * 写入缓存
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            ValueOperations<String, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, MINUTES_OF_ONE_DAY, TimeUnit.MINUTES);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 写入缓存
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public boolean set(final String key, Object value, Integer expireTime) {
        boolean result = false;

        try {
            ValueOperations<String, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }



    /**
     * 插入list
     * @param key
     * @param list
     * @param <T>
     */
    @Override
    public  <T> boolean setList(String key,List<T> list)
    {
        return setList(key,list,MINUTES_OF_ONE_DAY * 60);
    }


    @Override
    public <T> boolean setList(String key,List<T> list,Integer expireTime)
    {
        return set(key,list,expireTime);
    }
    /**
     * 取list
     * @param key
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> getList(String key)
    {
        Object jsonStr = get(key);

        if(jsonStr==null)
        {
            return null;
        }

        try {

            return (List<T>) jsonStr;

        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public <K,HK,HV> boolean setMap(K key, Map<HK, HV> map, Integer expireTime) {
        HashOperations<K, HK, HV> operations = redisTemplate.opsForHash();
        operations.putAll(key, map);

        if (expireTime != null) {
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
        }
        return false;
    }


    @Override
    public <K,HK,HV> Map<HK,HV> getMap(final K key) {
        HashOperations<K, HK, HV> operations = redisTemplate.opsForHash();
        return operations.entries(key);
    }
}
