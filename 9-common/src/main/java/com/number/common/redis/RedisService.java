package com.number.common.redis;

import java.util.List;
import java.util.Map;

/**
 * @author cao
 * @create 2018-03-31 下午6:16
 * redis服务接口
 **/
public interface RedisService {

    /**
     * 批量删除
     * @param keys key数组
     */
    public void remove(final String... keys);

    /**
     * 批量删除指定key
     * @param pattern
     */
    public void removePattern(final String pattern);

    /**
     * 删除指定key
     * @param key
     */
    public void remove(final String key);

    /**
     * 判断指定key是否存在
     * @param key
     * @return
     */
    public boolean exists(final String key);

    /**
     * 设置key的过期时间(秒)
     * @param key
     * @param expireTime
     */
    public void refreshExpireTime(final String key, Integer expireTime);

    /**
     * 获取指定key
     * @param key
     * @return
     */
    public Object get(final String key);

    /**
     * 添加key-value（使用默认失效时间1天）
     * @param key
     * @param value
     * @return
     */
    public boolean set(final String key, Object value);

    /**
     * 添加key-value（指定失效时间）
     * @param key
     * @param value
     * @param expireTime 失效时间（单位秒）
     * @return
     */
    public boolean set(final String key, Object value, Integer expireTime);


    /**
     * 插入list
     * @param key
     * @param list
     * @param <T>
     */
    <T> boolean setList(String key, List<T> list);

    /**
     * 插入list
     * @param key
     * @param list
     * @param expireTime
     * @param <T>
     */
    <T> boolean setList(String key, List<T> list, Integer expireTime);

    /**
     * 取list
     * @param key
     * @param <T>
     * @return
     */
    <T> List<T> getList(String key);

    /**
     * 存储map
     * @param key
     * @param map
     * @param expireTime 失效时间为null则永久生效（单位秒）
     * @return
     */
    public <K,HK,HV> boolean setMap(K key, Map<HK, HV> map, Integer expireTime);


    /**
     * 获取map
     * @param key
     * @param <K>
     * @param <HK>
     * @param <HV>
     * @return
     */
    public <K,HK,HV> Map<HK,HV> getMap(final K key);
}
