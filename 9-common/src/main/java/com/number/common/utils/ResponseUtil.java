package com.number.common.utils;

import com.number.common.enums.NmbExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;

import org.redisson.api.RedissonClient;


import javax.annotation.Resource;
import java.util.function.Supplier;

/**
 * @author jiang yong tao
 * @date 2018/10/25  14:29
 */
@Slf4j
@Deprecated
//@Service("ResponseUtil")
public class ResponseUtil {

    @Resource
    private RedissonClient redissonClient;

    private static final String TOKEN="_9_NUMBER";


    /**
     * 带锁方式执行返回
     * @param cacheKey
     * @param sup
     * @param <T>
     * @return
     */
    public <T> Response<T> excute(String cacheKey, Supplier<T> sup){
        if (StringUtils.isEmpty(cacheKey)){
            return Response.ofFail("请求参数错误");
        }
        Response<T> resp = new Response<>();
        log.info("返回数据 value = {}",sup.get());
        RLock rLock = redissonClient.getFairLock(TOKEN + cacheKey);
        try {
            resp.setResult(sup.get());
        }catch (Exception e){
            resp.setResponseEnum(NmbExceptionEnum.ResponseEnum.FAIL);
            log.warn("",e);
        }finally {
            rLock.unlock();
        }
        return resp;
    }


    /**
     * 不带锁方式执行返回
     * @param sup
     * @param <T>
     * @return
     */
    public <T> Response<T> excute(Supplier<T> sup){
        Response<T> resp = new Response<>();
        log.info("返回数据 value = {}",sup.get());
        try {
            resp.setResult(sup.get());
        }catch (Exception e){
            resp.setResponseEnum(NmbExceptionEnum.ResponseEnum.FAIL);
            log.warn("",e);
        }
        return resp;
    }
}
