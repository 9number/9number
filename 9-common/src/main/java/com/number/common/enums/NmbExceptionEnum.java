package com.number.common.enums;

import com.google.common.base.Preconditions;

/**
 * @author yefei
 * @date 2018-07-05 16:59
 */
public final class NmbExceptionEnum {


    public enum ResponseEnum {
        SUCCESS("2000", "成功", "调用成功"),
        FAIL("3000", "业务异常", "调用不合法！"),
        ERROR("4000", "未知错误，系统异常", "抱歉无法操作，请稍后再试！"),
        REPEAT("5000", "请勿重复提交", "请勿重复提交"),
        NO_LOGIN("6000", "未登录", "未登录"),
        ACCOUNTORPSWWRONG("6001","账号或密码错误,请重试","账号或密码错误,请重试"),
        SESSION_ID_OUT("6001","会话过期","会话过期"),
        ;

        public String code;

        public String message;

        public String tips;

        ResponseEnum(String code, String message, String tips) {

            this.code = code;
            this.message = message;
            this.tips = tips;
        }

    }
}
