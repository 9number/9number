package com.number.common.enums;

/**
 * @author TaoTao
 * @date 2019/07/06
 * Created By TaoTao On 2019/07/06
 */
public class   Constants {

    public enum Status{
        NORMAL(1,"正常"),
        DELETED(0,"删除")
        ;
        public int code;
        public String desc;
        public int getCode() {
            return code;
        }
        public String getDesc() {
            return desc;
        }
        Status(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }
    }

    public enum DetailType{
        DETAIL(1,"详情"),
        EDIT(0,"编辑")
        ;
        public int code;
        public String desc;
        public int getCode() {
            return code;
        }
        public String getDesc() {
            return desc;
        }
        DetailType(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }
    }



}
