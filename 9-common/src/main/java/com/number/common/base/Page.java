package com.number.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author jiang yong tao
 * @date 2018/11/9  10:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> implements Serializable {

    private static final long serialVersionUID = 1604192717721580125L;

    private Integer curr = 1;

    private Integer limit = 10;

    private List<T> data;

    private String msg;

    private int count;

    private Long code = 0L;

}
