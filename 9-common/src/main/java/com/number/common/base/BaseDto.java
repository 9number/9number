package com.number.common.base;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author TaoTao
 * @date 2019/06/15
 * Created By TaoTao On 2019/06/15
 */
@Setter
@Getter
public class BaseDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    private Integer curr = 1;

    private Integer limit = 10;

}
