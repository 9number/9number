package com.number.common.base;

import com.number.common.enums.NmbExceptionEnum;

/**
 * @author yefei
 * @date 2018-01-18 14:37
 */
public class BizException extends RuntimeException {

    private NmbExceptionEnum.ResponseEnum responseEnum;

    private Object result;

    public BizException() {
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(NmbExceptionEnum.ResponseEnum responseEnum) {
        super(responseEnum.tips);
        this.responseEnum = responseEnum;
    }

    public BizException(NmbExceptionEnum.ResponseEnum responseEnum, Object result) {
        super(responseEnum.tips);
        this.responseEnum = responseEnum;
        this.result = result;
    }

    public NmbExceptionEnum.ResponseEnum getResponseEnum() {
        return responseEnum;
    }

    public Object getResult() {
        return result;
    }
}
