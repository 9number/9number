package com.number.common.base;

import com.alibaba.fastjson.JSON;
import com.number.common.enums.NmbExceptionEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * @author TaoTao
 * @date 2019/06/15
 * Created By TaoTao On 2019/06/15
 */
@Data
@Slf4j
public class Response<T> implements Serializable {

    private String code;

    private String msg;

    private T result;

    private T data;


    public Response() {
        this.code = NmbExceptionEnum.ResponseEnum.SUCCESS.code;
        this.msg = NmbExceptionEnum.ResponseEnum.SUCCESS.message;
    }


    public void setResponsEnum(NmbExceptionEnum.ResponseEnum response){
        this.code = response.code;
        this.msg = response.message;
    }


    public static <T> Response<T> doResponse(Supplier<T> fun){
        Response<T> response = new Response<>();
        T res = fun.get();
        if (res!=null){
            log.info("返回值为result ====== {} ", JSON.toJSONString(res));
        }
        try {
            response.setResult(res);
            response.setResponsEnum(NmbExceptionEnum.ResponseEnum.SUCCESS);
        } catch (Exception e) {
            response.setResponsEnum(NmbExceptionEnum.ResponseEnum.FAIL);
            e.printStackTrace();
        }
        return response;
    }
}
