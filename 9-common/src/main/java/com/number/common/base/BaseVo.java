package com.number.common.base;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author jiang yong tao
 * @date 2018/10/24  10:40
 */
public class BaseVo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
