package com.number.user.services.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.number.common.enums.Constants;
import com.number.common.utils.DTOUtils;
import com.number.user.dto.MenuDto;
import com.number.user.mapper.MenuMapper;
import com.number.user.mapper.RoleMenuMapper;
import com.number.user.model.Menu;
import com.number.user.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
@Service(version = "1.0.0",interfaceName = "MenuService")
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public Boolean inserMenu(MenuDto menuDto) {
        Menu menu = DTOUtils.map(menuDto,Menu.class);
        return menuMapper.insert(menu) > 0 ;
    }

    @Override
    public Boolean updateMenu(MenuDto menuDto) {
        Menu menu = DTOUtils.map(menuDto,Menu.class);
        return menuMapper.updateByPrimaryKeySelective(menu) > 0;
    }

    @Override
    public List<MenuDto> queryMenuAllList() {
        List<MenuDto> parentMenu = queryMenuListByPid(-1L).stream().peek(v->{
            v.setPtitle(queryMenuById(v.getPid()).getTitle());
            List<MenuDto> childmenus =  queryMenuListByPid(v.getId()).stream().peek(a->{
                a.setPtitle(queryMenuById(a.getPid()).getTitle());
            }).collect(toList());
            v.setChildren(childmenus);
        }).collect(toList());
        return parentMenu;
    }

    @Override
    public List<MenuDto> queryMenuListByRoleId(Long roleId) {
//        List<MenuDto> menus = new ArrayList<>();
//        Example roleMenuExample = new Example(RoleMenu.class);
//        roleMenuExample.and().andEqualTo("roleId",roleId);
//        List<Long> menuIds = roleMenuMapper.selectByExample(roleMenuExample).stream().map(RoleMenu::getMenuId).collect(toList());
//        if (menuIds.size()>0){
//            Example menuExample = new Example(Menu.class);
//            menuExample.and().andIn("id",menuIds);
//            menus = DTOUtils.map(menuMapper.selectByExample(menuExample),MenuDto.class);
//        }

        return null;
    }

    @Override
    public List<MenuDto> queryMenuListByPid(Long parentId) {
        Example example = new Example(Menu.class);
        example.and().andEqualTo("pid",parentId).andEqualTo("status", Constants.Status.NORMAL.code);
        List<Menu> menus = menuMapper.selectByExample(example);
        return DTOUtils.map(menus,MenuDto.class);
    }

    @Override
    public MenuDto queryMenuById(Long id) {
        Menu menu = menuMapper.selectByPrimaryKey(id);
        return DTOUtils.map(menu,MenuDto.class);
    }

    @Override
    public Menu queryMenuByCode(String code) {
        Example example = new Example(Menu.class);
        example.and().andEqualTo("code",code).andEqualTo("status", Constants.Status.NORMAL.code);
        Menu menu = menuMapper.selectOneByExample(example);
        return menu;
    }


}
