package com.number.user.services;

import com.number.user.dto.MenuDto;
import com.number.user.model.Menu;

import java.util.List;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
public interface MenuService {

    /**
     * 新增菜单
     * @param menuDto 菜单
     * @return 是否新增成功
     */
    Boolean inserMenu(MenuDto menuDto);

    /**
     * 更新菜单
     * @param menuDto 菜单
     * @return 是否更新成功
     */
    Boolean updateMenu(MenuDto menuDto);

    /**
     * 查询全部菜单(树形，递归)
     * @return 菜单树形列表
     */
    List<MenuDto> queryMenuAllList();

    /**
     * 根据角色ID查询菜单
     * @param roleId 角色ID
     * @return 菜单信息
     */
    List<MenuDto> queryMenuListByRoleId(Long roleId);

    /**
     * 根据父级ID查询菜单
     * @param parentId 父级ID
     * @return 菜单列表
     */
    List<MenuDto> queryMenuListByPid(Long parentId);

    /**
     * 根据ID查询菜单信息
     * @param id 菜单ID
     * @return 菜单信息
     */
    MenuDto queryMenuById(Long id);

    /**
     * 根据菜单编码查询菜单
     * @param code 菜单编码(唯一)
     * @return 菜单信息
     */
    Menu queryMenuByCode(String code);

}
