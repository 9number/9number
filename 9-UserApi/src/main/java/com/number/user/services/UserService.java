package com.number.user.services;

import com.number.common.base.Page;
import com.number.user.dto.UserDto;
import com.number.user.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;


/**
 * @author TaoTao
 * @date 2018/09/17
 * Created By TaoTao On 2018/09/17
 */
public interface UserService {

    User queryUserByAcountAndPsw(String acount, String psw);

    User queryByUserId(Long userid);

    Page<UserDto> queryUserList(UserDto userDto);

    Boolean addManager(User user);

    Boolean updateUser(User user);
}
