package com.number.user.services.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.number.common.base.Page;
import com.number.common.utils.DTOUtils;
import com.number.common.utils.MD5Util;
import com.number.user.config.SysParams;
import com.number.user.dto.UserDto;
import com.number.user.mapper.UserMapper;
import com.number.user.model.User;
import com.number.user.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author TaoTao
 * @date 2018/09/17
 * Created By TaoTao On 2018/09/17
 */
@Slf4j
@Service(version = "1.0.0")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SysParams sysParams;

    @Override
    public User queryUserByAcountAndPsw(String acount, String psw) {
        Example example = new Example(User.class);
        example.and().andEqualTo("userName",acount).andEqualTo("userPsw",psw);
        User user = userMapper.selectOneByExample(example);
        return user;
    }

    @Override
    public User queryByUserId(Long userid) {
        User user = userMapper.selectByPrimaryKey(userid);
        return user;
    }

    @Override
    public Page<UserDto> queryUserList(UserDto userDto) {

        Example example = new Example(User.class);
        if (Objects.nonNull(userDto.getUserName())){
            example.and().andEqualTo("userName",userDto.getUserName());
        }
        if (Objects.nonNull(userDto.getStartTime())){
            example.and().andGreaterThanOrEqualTo("createTime",userDto.getStartTime());
        }
        if (Objects.nonNull(userDto.getEndTime())){
            example.and().andLessThanOrEqualTo("createTime",userDto.getEndTime());
        }
        PageHelper.startPage(userDto.getCurr(),userDto.getLimit());
        List<User> userList = userMapper.selectByExample(example);
        int count = userMapper.selectCountByExample(example);
        List<UserDto> userDtos = DTOUtils.map(userList,UserDto.class);

        Page<UserDto> userPage = new Page<>();


        userPage.setData(userDtos);
        userPage.setCount(count);
        return userPage;
    }

    @Override
    public Boolean addManager(User user) {

        String psw = MD5Util.encrypt("12345");
        user.setUserPsw(psw);
        user.setStatus(1);
        user.setCreateTime(new Date());
        user.setCreator("admin");
        user.setUpdator("admin");
        return userMapper.insertSelective(user)==1;
    }


    @Override
    public Boolean updateUser(User user) {
        return Boolean.TRUE;
    }
}
