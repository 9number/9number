package com.number.user.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author jiang yong tao
 * @date 2019/7/22 10:20
 */
@Component
@Getter
public class SysParams {

    @Value("${upload_img_url}")
    private String headImgPath;

}
