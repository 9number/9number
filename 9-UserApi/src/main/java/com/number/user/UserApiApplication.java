package com.number.user;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;


/**
 * @author TaoTao
 * @date 2018/09/17
 * Created By TaoTao On 2018/09/17
 */
@MapperScan("com.number.user.mapper")
@SpringBootApplication(scanBasePackages = "com.number")
@EnableDubbo
public class UserApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApiApplication.class);
    }
}
