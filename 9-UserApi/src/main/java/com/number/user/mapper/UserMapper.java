package com.number.user.mapper;

import com.number.user.model.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author TaoTao
 */
@org.apache.ibatis.annotations.Mapper
public interface UserMapper extends Mapper<User> {}