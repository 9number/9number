package com.number.user.mapper;

import com.number.user.base.MyMapper;
import com.number.user.model.RoleMenu;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
public interface RoleMenuMapper extends MyMapper<RoleMenu> {
}
