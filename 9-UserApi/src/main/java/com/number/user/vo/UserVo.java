package com.number.user.vo;

import com.number.common.base.BaseVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TaoTao
 * @date 2018/09/18
 * Created By TaoTao On 2018/09/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserVo extends BaseVo {

    private Long uid;

    private Long roleId;

    private String loginName;

    private String userHeadImg;

    private String sid;

}
