package com.number.user.dto;

import com.number.common.base.BaseDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleMenuDto extends BaseDto {

    private Long userId;

    private Long roleId;

    private Long menuId;
}
