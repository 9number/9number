package com.number.user.dto;

import com.number.common.base.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.UtilityClass;

import java.beans.ConstructorProperties;
import java.util.List;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("菜单")
public class MenuDto extends BaseDto {

    @ApiModelProperty("菜单ID")
    private Long id;

    @ApiModelProperty(value = "父级菜单ID",example = "1")
    private Long pid;

    @ApiModelProperty("父级菜单名称")
    private String ptitle;

    @ApiModelProperty("节点初始化是否展开")
    private boolean spread = false;

    @ApiModelProperty("菜单名称")
    private String title;

    @ApiModelProperty("菜单URL")
    private String href;

    @ApiModelProperty(value = "菜单序号",example = "123")
    private Integer seqno;

    @ApiModelProperty("节点初始化是否被选中")
    private boolean checked;

    @ApiModelProperty("节点初始化是否禁用状态")
    private boolean disabled = false;

    @ApiModelProperty("节点初始化是否禁用状态")
    private String icon;

    @ApiModelProperty("子菜单")
    private List<MenuDto> children;


}
