package com.number.user.dto;

import com.number.common.base.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

/**
 * @author TaoTao
 * @date
 * @apiNote
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@ApiModel("用户")
public class UserDto extends BaseDto {

    @ApiModelProperty("用户ID")
    private Long id;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户密码")
    private String userPsw;

    @ApiModelProperty("用户头像")
    private String userHeadImg;

    @ApiModelProperty("用户手机号")
    private String userMobile;

    @ApiModelProperty("用户角色")
    private Long userRole;

    @ApiModelProperty(value = "用户状态(1,正常;0,删除)",example = "0")
    private Integer status;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("创建者")
    private String creator;

    @ApiModelProperty("变更者")
    private String updator;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

}