package com.number.user.model;

import com.number.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "nmb_role_menu")
public class RoleMenu extends BaseModel {

    private Long id;

    private Long roleId;

    private Long menuId;

}
