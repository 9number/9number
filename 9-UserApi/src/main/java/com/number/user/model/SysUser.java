package com.number.user.model;

import com.number.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_user")
public class SysUser extends BaseModel {

    private Long id;

    private String sysName;

    private String sysPsw;

    private Long roleId;

    private Long status;

    private Date createTime;

    private Date updateTime;
}
