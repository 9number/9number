package com.number.user.model;

import com.number.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "nmb_menu")
public class Menu extends BaseModel {

    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    private Long pid;

    private String title;

    private String href;

    private String icon;

    private Integer seqno;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private String creator;

    private String updator;
}