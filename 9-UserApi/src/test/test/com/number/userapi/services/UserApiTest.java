package com.number.userapi.services;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.number.common.base.Page;
import com.number.user.dto.UserDto;
import com.number.user.services.UserService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.StringJoiner;


/**
 * @author jiang yong tao
 * @date 2018/10/24  9:20
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserApiTest {
    @Resource
    private UserService userService;

    private static List<Book> books = Lists.newArrayList(new Book("QQ",1),
            new Book("WW",2),
            new Book("EE",3),
            new Book("FF",4),
            new Book("DD",5)
            );


    @Data
    @AllArgsConstructor
    static
    class Book{

        private String name;

        private Integer age;

        @Override
        public String toString() {
            return new StringJoiner(", ", Book.class.getSimpleName() + "[", "]")
                    .add("name='" + name + "'")
                    .add("age=" + age)
                    .toString();
        }
    }

    @Test
    public void queryUserList(){
        UserDto page = new UserDto();
        page.setCurr(1);
        Page<UserDto> userVos = userService.queryUserList(page);
        System.out.println(JSON.toJSONString(userVos));
    }

    public static void main(String[] args) {
        String server = "106.15.195.87:2181";
        ZkClient zk = new ZkClient(server,10000,10000,new SerializableSerializer());
        System.out.println("Connection success!");
        zk.delete("/user");
//        UserDto userDto = UserDto.builder().userName("姜永涛").userPsw("123456").status(1).build();
//        String path = zk.create("/user",userDto,CreateMode.PERSISTENT);
//        System.out.println(path);
//        Runnable rn = ()->{
//            Stat stat = new Stat();
//             UserDto child = zk.readData("/user",stat);
//            System.out.println(JSON.toJSONString(child));
//            System.out.println(stat);
//            System.out.println(zk.getCreationTime("/user"));
//        };
//        rn.run();


    }
}
