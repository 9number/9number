package com.number.web.base;

import com.number.user.vo.UserVo;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
public class ThreadLcal {

    private final static ThreadLocal<UserVo> loginUserInfo = new ThreadLocal<>();

    public static Long getUserId(){
        return getCurrentUser().getUid();
    }

    public static UserVo getCurrentUser() {
        return loginUserInfo.get();
    }

    public static void setCurrentUser(UserVo value) {
        loginUserInfo.set(value);
    }

    public static void removeCurrentUser() {
        loginUserInfo.remove();
    }
}
