package com.number.web.annotation;

import java.lang.annotation.*;

/**
 * @author jiang yong tao
 * @date 2019/6/20 17:10
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Promise {
    String name() default "";
}
