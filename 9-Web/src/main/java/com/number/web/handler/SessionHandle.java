package com.number.web.handler;

import com.number.user.vo.UserVo;
import com.number.web.annotation.SessionIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Objects;

import static com.number.web.base.ThreadLcal.setCurrentUser;


/**
 * @author TaoTao
 * @date 2018/09/18
 * Created By TaoTao On 2018/09/18
 */
public class SessionHandle implements HandlerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod){
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            logger.info("reqMethod : {}",handlerMethod.getMethod());
            Enumeration<String> params = request.getParameterNames();
            while (params.hasMoreElements()){
                logger.info("请求参数 ：{}", request.getParameter(params.nextElement()));
            }
            //检查忽略登陆
            SessionIgnore sessionIgnore = handlerMethod.getMethodAnnotation(SessionIgnore.class);
            if (sessionIgnore!=null){
                return true;
            }else {
                String sid = request.getParameter("sid");
                if (Objects.isNull(sid)){
                    response.sendRedirect("/console");
                    return true;
                }else {
                    UserVo userVo = (UserVo) request.getSession().getAttribute("userInfo");
                    if (Objects.nonNull(userVo)){
                        setCurrentUser(userVo);
                    }else {
                        response.sendRedirect("/console");
                        return true;
                    }
                    //检查权限
//                    Promise promise = handlerMethod.getMethodAnnotation(Promise.class);
//                    if (Objects.nonNull(promise)){
//                        String promiseName = promise.name();
//                        return true;
//                    }
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        logger.info("HeaderNames = {}",response.getHeaderNames());
        logger.info("响应状态码 Status = {}",response.getStatus());
        logger.info("============================= 完成! 用时:");
    }


//    @Deprecated
//    private boolean checkHavePromise(HttpServletRequest request,String promiseName){
//        Menu menu = null;
//        if (menuService == null){
//            BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
//            menuService = (MenuService) factory.getBean("MenuService");
//            menu = menuService.queryMenuByCode(promiseName);
//        }
//        return !Objects.isNull(menu);
//    }
}
