package com.number.web.handler;

import com.number.common.base.BizException;
import com.number.common.base.Response;
import com.number.common.enums.NmbExceptionEnum;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author TaoTao
 * @date 2019/06/15
 * Created By TaoTao On 2019/06/15
 */
@RestControllerAdvice
public class ExceptionHandle  {


    @ExceptionHandler(BizException.class)
    public Response exceptionHandle(Exception e){
        e.printStackTrace();
        Response response = new Response();
        if (e instanceof BizException){
            NmbExceptionEnum.ResponseEnum responseEnum = ((BizException) e).getResponseEnum();
            response.setResponsEnum(responseEnum);
        }
        return response;
    }
}
