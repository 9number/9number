package com.number.web.handler;

/**
 * @author TaoTao
 * @date 2019/06/19
 * Created By TaoTao On 2019/06/19
 */
public class SessionModel {
    public static final String USER_PREFIX = "USER_ID";
    public static final String TOKEN_PREFIX = "token";
    public static final String DEVICE_ID = "DEVICE_ID";

}
