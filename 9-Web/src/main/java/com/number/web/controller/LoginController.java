package com.number.web.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.number.common.base.BizException;
import com.number.common.base.Response;
import com.number.common.redis.RedisService;
import com.number.common.utils.MD5Util;
import com.number.user.model.User;
import com.number.user.services.UserService;
import com.number.user.vo.UserVo;
import com.number.web.annotation.SessionIgnore;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static com.number.common.enums.NmbExceptionEnum.ResponseEnum.ACCOUNTORPSWWRONG;
import static com.number.web.base.ThreadLcal.removeCurrentUser;
import static com.number.web.base.ThreadLcal.setCurrentUser;

/**
 * @author jiang yong tao
 * @date 2019/7/12 10:41
 */
@Controller
public class LoginController {

    @Reference(version = "1.0.0",timeout = 10000,check = false)
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @ApiOperation(value = "全局跳转",httpMethod = "GET")
    @GetMapping("console")
    @SessionIgnore
    public ModelAndView console(ModelAndView modelAndView){
        modelAndView.setViewName("login");
        return modelAndView;
    }


    @ApiOperation(value = "登录",httpMethod = "GET",notes = "登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName",dataType = "String",required = true),
            @ApiImplicitParam(name = "psw",required = true,dataType = "String")
    })
    @SessionIgnore
    @PostMapping("login")
    @ResponseBody
    public Response<UserVo> login(HttpServletRequest request, @RequestParam()String userName, @RequestParam()String psw){
        return Response.doResponse(()->{
            String password = MD5Util.encrypt(psw);
            User user = userService.queryUserByAcountAndPsw(userName,password);
            if (user!=null){
                String sessionId = request.getSession().getId();
                UserVo userVo = UserVo.builder()
                        .loginName(user.getUserName())
                        .userHeadImg(user.getUserHeadImg())
                        .uid(user.getId())
                        .sid(sessionId)
                        .roleId(user.getUserRole())
                        .build();
                request.getSession().setAttribute("sid",sessionId);
                request.getSession().setAttribute("userInfo",userVo);
                setCurrentUser(userVo);
                return userVo;
            }else {
                throw new BizException(ACCOUNTORPSWWRONG);
            }
        });
    }

    @GetMapping("layOut")
    public ModelAndView layOut(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        removeCurrentUser();
        request.getSession().removeAttribute("sid");
        request.getSession().removeAttribute("userInfo");
        modelAndView.setViewName("login");
        return modelAndView;
    }
}
