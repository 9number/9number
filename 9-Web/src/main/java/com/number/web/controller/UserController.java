package com.number.web.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.number.common.base.Page;
import com.number.common.base.Response;
import com.number.common.enums.Constants;
import com.number.user.dto.UserDto;
import com.number.user.model.User;
import com.number.user.services.UserService;
import com.number.web.annotation.Promise;
import com.number.web.annotation.SessionIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author TaoTao
 * @date 2018/09/17
 * Created By TaoTao On 2018/09/17
 */
@Api("用户接口")
@Controller
@RequestMapping("/user/")
public class UserController {

    @Reference(version = "1.0.0",timeout = 20000,check = false)
    private UserService userService;

    @Value("${locationImg}")
    private String locationImg;

    private final static List<String> IMG_TYPES = Lists.newArrayList(".jpg",".png",".gif",".jpeg");


    @ApiOperation(value = "跳转详情Iframe",httpMethod = "GET",notes = "用户信息")
    @Promise(name = "user:detail")
    @GetMapping("detail")
    public ModelAndView getUserDetail(ModelAndView modelAndView,Long userId, Integer type){
        User user = userService.queryByUserId(userId);
        modelAndView.addObject("user",user);
        if (Constants.DetailType.EDIT.code == type){
            modelAndView.setViewName("user/edituser");
        }
        if (Constants.DetailType.DETAIL.code == type){
            modelAndView.setViewName("user/userdetail");
        }
        return modelAndView;
    }

    @ApiOperation(value = "跳转新增Iframe",httpMethod = "GET",notes = "跳转新增页面")
    @Promise(name = "user:add")
    @GetMapping("toaddpage")
    public ModelAndView toAddPage(ModelAndView view){
        view.setViewName("user/adduser");
        return view;
    }

    @ApiOperation(value = "跳转用户列表页",httpMethod = "GET",notes = "用户列表页")
    @Promise(name = "user:manage")
    @GetMapping("manage")
    public ModelAndView console(ModelAndView view){
        view.setViewName("user/userlist");
        return view;
    }


    @ApiOperation(value = "分页查询用户列表",httpMethod = "GET",notes = "分页查询用户列表")
    @RequestMapping("list")
    @ResponseBody
    public Page<UserDto> queryUserList(UserDto userDto){
        Page<UserDto> userList = userService.queryUserList(userDto);
            return userList;
    }

    @ApiOperation(value = "新增用户",httpMethod = "POST",notes = "新增用户")
    @Promise(name = "user:add")
    @PostMapping(value = "add")
    @ResponseBody
    public Response<Boolean> addManager(User user){
        return Response.doResponse(()->{
            Boolean flag = userService.addManager(user);
            return flag;
        });
    }

    @ApiOperation(value = "更新用户",httpMethod = "POST",notes = "根据用户ID更新用户")
    @Promise(name = "user:update")
    @PostMapping("updateUser")
    public Response<Boolean> updateUser(User user){
        return Response.doResponse(()->{
            Boolean flag = userService.updateUser(user);
            return flag;
        });
    }


    @SessionIgnore
    @RequestMapping("uploadHeadImg")
    @ResponseBody
    public JSONObject uploadHeadImg(@RequestParam(value = "file")MultipartFile file){
        JSONObject result = new JSONObject();
                if (file.isEmpty() || StringUtils.isEmpty(file.getOriginalFilename())){
                    result.put("code",1);
                    result.put("msg","请选择文件");
                    return result;
                }
                String path = locationImg;
                long fileName = System.currentTimeMillis();
                String orignName = file.getOriginalFilename();
                String extName = orignName.substring(orignName.lastIndexOf("."));
                if (!IMG_TYPES.contains(extName)){
                    result.put("code",1);
                    result.put("msg","请选择正确格式的图片");
                    return result;
                }
                File targetFile = new File(path + File.separator + fileName + extName);
                if(!targetFile.getParentFile().exists()){
                    targetFile.getParentFile().mkdirs();
                }
                try {
                    JSONObject src = new JSONObject(1);
                    file.transferTo(targetFile);
                    src.put("src",path + fileName+extName);
                    result.put("data",src);
                    result.put("code",0);
                    result.put("msg","上传头像成功");
                } catch (IOException e) {
                    e.printStackTrace();
                    result.put("code",1);
                    result.put("msg","上传头像失败");
                }
        return result;
    }
}
