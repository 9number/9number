package com.number.web.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.number.common.base.Response;
import com.number.user.dto.MenuDto;
import com.number.user.services.MenuService;
import com.number.web.annotation.Promise;
import com.number.web.annotation.SessionIgnore;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author TaoTao
 * @date 2019/06/16
 * Created By TaoTao On 2019/06/16
 */
@Controller
@RequestMapping("/menu/")
public class MenuController {
    @Reference(version = "1.0.0",timeout = 10000,check = false)
    private MenuService menuService;

    @ApiOperation("树形菜单列表")
    @Promise(name = "menu:menuTree")
    @GetMapping("menu_tree")
    @ResponseBody
    public List<MenuDto> queryAllMenu(){
        List<MenuDto> menuDtos = menuService.queryMenuAllList();
            return menuDtos;
    }

    @ApiOperation("查询所有菜单")
    @GetMapping("list")
    public ModelAndView queryMenuList(){
        ModelAndView modelAndView = new ModelAndView("home");
        List<MenuDto> allMenus = menuService.queryMenuAllList();
        modelAndView.addObject("allMenus",allMenus);
        return modelAndView;
    }

    @ApiOperation(value = "跳转菜单列表页",httpMethod = "GET")
    @SessionIgnore
    @Promise(name = "menu:manage")
    @GetMapping("manege")
    public ModelAndView menuManage(){
        ModelAndView modelAndView = new ModelAndView("menu/menulist");
        return modelAndView;
    }

    @ApiOperation(value = "编辑菜单",httpMethod = "POST")
    @Promise(name = "menu:update")
    @GetMapping("update")
    public Response<Boolean> menuUpdate(MenuDto menuDto){
        return Response.doResponse(()->{
            return menuService.updateMenu(menuDto);
        });
    }


    @ApiOperation(value = "新增菜单",httpMethod = "POST")
    @Promise(name = "menu:add")
    @GetMapping("add")
    public Response<Boolean> menuAdd(MenuDto menuDto){
        return Response.doResponse(()->{
           return menuService.inserMenu(menuDto);
        });
    }

}
