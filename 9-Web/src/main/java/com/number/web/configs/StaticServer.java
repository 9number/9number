//package com.number.web.configs;
//
//import io.vertx.ext.web.Router;
//import io.vertx.core.AbstractVerticle;
//import io.vertx.ext.web.handler.StaticHandler;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * @author jiang yong tao
// * @date 2018/10/22  15:07
// */
//@Deprecated
//@Component
//public class StaticServer extends AbstractVerticle {
//    @Autowired
//    VertConfiguration configuration;
//
//    @Override
//    public void start() throws Exception {
//        Router router = Router.router(vertx);
//
//        // Serve the static pages
//        router.route().handler(StaticHandler.create());
//
//        vertx.createHttpServer().requestHandler(router::accept).listen(configuration.httpPort());
//    }
//}
