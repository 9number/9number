package com.number.web.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author jiang yong tao
 * @date 2018/10/22  15:04
 */
@Configuration
public class VertConfiguration {
    @Autowired
    private Environment environment;

    public int httpPort() {
        return environment.getProperty("http.port", Integer.class, 8081);
    }
}
