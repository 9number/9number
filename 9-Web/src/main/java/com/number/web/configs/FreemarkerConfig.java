//package com.number.web.configs;
//
//import freemarker.ext.jsp.TaglibFactory;
//import freemarker.template.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
//
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.util.Map;
//
///**
// * @author jiang yong tao
// * @date 2018/11/2  16:17
// */
////@Component
//public class FreemarkerConfig<T> {
//
//    @Autowired
//    private FreeMarkerConfigurer freeMarkerConfigurer;
//
//    @Bean
//    public Configuration getFreeMarkerConfiguration(){
//        return freeMarkerConfigurer.getConfiguration();
//    }
//
//    public void resolveMap(Map<String,String> model, String templateName){
//        try {
//            Template template = this.getFreeMarkerConfiguration().getTemplate(templateName);
//            template.process(model, new OutputStreamWriter(System.out));
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (TemplateException e) {
//            e.printStackTrace();
//        }
//    }
//}
