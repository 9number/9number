package com.number.web;

import com.alibaba.druid.mock.MockResultSet;
import com.alibaba.fastjson.JSON;
import com.number.user.model.User;
import com.number.user.vo.UserVo;
import com.number.web.controller.MenuController;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.checkerframework.checker.units.qual.A;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.number.web.base.ThreadLcal.setCurrentUser;


/**
 * @author jiang yong tao
 * @date 2018/10/24  15:32
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDemo {

    @Autowired
    private MenuController menuController;

    @Autowired
    private HttpServletRequest request;

    private MockMvc mockMvc;

    @Before
    public void setUp(){
        UserVo userVo = UserVo.builder().loginName("admin").roleId(1L).uid(2L).build();
        setCurrentUser(userVo);
        mockMvc = MockMvcBuilders.standaloneSetup(menuController).build();
    }


    @Test
    public void queryMenuList() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.post("/queryAllMenu")).andDo(MockMvcResultHandlers.print());
        System.out.println();
    }

}
