package com.number.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.number.user.dto.MenuDto;
import com.number.faced.services.MenuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jiang yong tao
 * @date 2019/6/18 14:46
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuServiceTest {

    @Reference(version = "1.0.0",timeout = 10000,check = false)
    private MenuService menuService;

    @Test
    public void queryAllMenus(){
        List<MenuDto> menuDtos = menuService.queryMenuAllList();
        System.out.println(JSON.toJSONString(menuDtos));;
    }



}
